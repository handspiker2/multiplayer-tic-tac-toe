//Imports
var ip = require("ip");
var socket = require("socket.io");
var express = require("express");
//Make sure we have the game object
var Game = require("./Game.js");

var server = express.createServer();
var io = socket.listen(server);

server.use("/", express.static(__dirname + '/../public'));
server.use("/public", express.static(__dirname + '/../public'));
//Add the render
server.get("/", function(req, res){
	res.render(__dirname + '/views/game.ejs', {
		styleVersion: process.env.styleVersion || '',
		layout:false,
		locals:{ip: ip.address()}
	});
});

io.set("log level",1);

var session = [];

io.sockets.on("connection", function(client){
	console.log("New Connection!");

	var newGame = true;

	for (var i = 0;i<session.length;i++){

		var g = session[i];
		//Does the game need more players
		if(!g.client1 || !g.client2){
			if(g.client1 == null){
				console.log("JOINED GAME #" + i + " as PLAYER 1");
				//Set as player 1
				g.client1 = client;
			}else{
				console.log("JOINED GAME #" + i + " as PLAYER 2");
				//Set as player 2
				g.client2 = client;
			}
			//If there are now two players, start the game
			if(g.client1 && g.client2){
				g.reset();
				//Tell them to make sure the board is empty
				var passback = {};
				passback.board = g.board;
				g.client1.emit("updateBoard", passback);
				g.client2.emit("updateBoard", passback);

				g.client1.emit("start", "X");
				g.client2.emit("start", "O");

			}	
			//Track what game they are in
			client.game = g;
			//We don't need a new game
			newGame = false;
			//No need no search anymore
			break;
		}
	}

	//Do we create a new game
	if(newGame){
		console.log("Created new game");
		var room = new Game();
		room.client1 = client;
		client.game = room;
		session.push(room);
	}
	///////////////////////////////////////////////////////////////////////////////////////EVENTS
	client.on("message", function(message){

		if(message.trim() == ""){
			return;
		}

		console.log("GAME #" + client.game +" MESSAGE: " + message);

		//HTML Escape
		message = message.replace("&", "&amp;").replace("<", "&lt;").replace(">", "&gt;").replace("\"", "&quot;").replace("\'", "&apos;");

		//Send message to other client in game
		if((client.game.client2 != null) && client.game.client1 == client){
			client.game.client2.emit("chat", "OPPONENT: " + message);
		}else if((client.game.client1 != null) && client.game.client2 == client){
			client.game.client1.emit("chat", "OPPONENT: " + message);
		}

		//Send back to the client (AKA proving the server got the message);
		client.emit("chat", "YOU: "+ message);
	});

	client.on("place",function (click) {
		var passback = {};
		//Include the board
		passback.board = client.game.board;
		var game = client.game;

		var player;
		if(game.client1 == client){
			player = Game.PLAYER_X;
		}else{
			player = Game.PLAYER_Y;
		}

		var isPlaced = game.place(player, click.x,click.y);

		if(isPlaced){
				
			//Did someone win?
			if(isPlaced === "won"){
				//alert the clients
				game.client1.emit("gameover", client.game.winner);
				game.client2.emit("gameover", client.game.winner);
				passback.turn = "X";
			}else{
				//Is the board full?
				var full = true;
				for(var x=0;x<game.board.length;x++){
					for(var y=0;y<game.board[x].length;y++){

						//If nothing in the cell the board is not full
						if(game.board[x][y] === 0){
							full = false;
						}
					}
				}

					//Who's turn is it now?
				if(player == Game.PLAYER_X){
					passback.turn = "O";
				}else{
					passback.turn = "X";
				}


				if(full){
					game.client1.emit("gameover", 0);
					game.client2.emit("gameover", 0);
					game.reset();
					passback.turn = "X";
				}
			}

			//Tell clients to update the board
			game.client1.emit("updateBoard", passback);
			game.client2.emit("updateBoard", passback);

		}else{
			console.log("GAME #" + client.game +" Invalid move placed!");
		}
		
	});

	client.on('disconnect', function () {
        if(client.game.client2 == client){
        	if(client.game.client1){
	        	//Let the other player know
	        	client.game.client1.emit("playerLeft");
	        }
	        //Remove refences to this client
        	client.game.client2 = null;

        }else if(client.game.client1 == client){
        	if(client.game.client2){
	        	//Let the other player know
	        	client.game.client2.emit("playerLeft");
	        }
	        //Remove refences to this client
        	client.game.client1 = null;
        }

        //Clean up empty games
        for (var i=0; i < session.length;i++) {
        	if (!session[i].client1 && !session[i].client2) {
        		session = session.splice(i,1);
        	}
        };
    });

    //Force them to reset the board
	var passback = {};
	passback.board = client.game.board;
	client.emit("updateBoard", passback);

	if(client.game.client1 && client.game.client2){

		client.game.client1.emit("start", "X");
		client.game.client2.emit("start", "O");

	}	

	console.log("Current games: " + session.length);
});

var port = Number(process.env.PORT || 8080);
console.log('Listening on ' + port);
server.listen(port);