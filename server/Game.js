function Game(){

//-----------------------------CONSTRUCTOR
	this.winner = 0;
	//Turns
	this.turn = -1;

	this.client1 = null;
	this.client2 = null;
	//Place to store the board
	this.board = [];
	//Create the rows
	this.board[0] = [0,0,0];
	this.board[1] = [0,0,0];
	this.board[2] = [0,0,0];

	//------------------------PRIVATE FUNCTIONS
	this.getRowTotal = function(row){
		var output = 0;

		for(var colNum = 0;colNum< 3;colNum++){
			output += this.board[colNum][row];
		}

		return output;
	}
	this.getColTotal = function(colNum){
		var output = 0;

		for(var row = 0;row< 3;row++){
			output += this.board[colNum][row];
		}

		return output;
	}

	this.winCheck = function (player) {
		var total = (player*3);

		var backslash = this.board[1][1] + this.board[0][2] + this.board[2][0];
		var forwardslash = this.board[2][2] + this.board[0][0] + this.board[1][1];

		if((this.getRowTotal(0) == total ||this.getRowTotal(1) == total ||this.getRowTotal(2) == total) || //Check rows
			(this.getColTotal(0) == total ||this.getColTotal(1) == total ||this.getColTotal(2) == total) || //Check Columns
			forwardslash == total || backslash == total 									//Check Diangles
		){
			this.reset();
			return true;
		}else{
			return false;
		}
	}

////-----------------------CONSTRUCTOR END

	this.reset = function () {
		this.turn = Game.PLAYER_X;
		//Reset board
		this.board[0] = [0,0,0];
		this.board[1] = [0,0,0];
		this.board[2] = [0,0,0];
		//reset wiiner
		this.winner = 0;
	}

	this.place = function (player, x, y) {
		//Is it player's turn
		if(player == this.turn){

			//is a piece there
			if(this.board[x][y] !==0){
				return false;
			}
			//Place the piece
			this.board[x][y] = player;

			//check if it causes a win
			if(this.winCheck(player)){
				this.winner = player;
				return "won";
			}

			if(player == Game.PLAYER_X){
				this.turn = Game.PLAYER_Y;

			}else if(player == Game.PLAYER_Y){
				this.turn = Game.PLAYER_X;
			}


			//Placed fine
			return true;
		}else{
			//Wasn't their turn
			return false;
		}
		
	}
};

//-----------------------------------------CONSTANTS
Game.PLAYER_X = -1;
Game.PLAYER_Y = 1;

//------------export object
module.exports = Game;
